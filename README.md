# README #

This repo uses Terraform CDK to build a two tier web application infrastructure. 

### Prerequisites ###

To run the infrastructure, just need to clone the repository and run Terraform Commands, example:

$ terraform apply 

Terraform will read the json configuration file cdk.tf.json which has been created by the Terraform CDK runing main.py

In case you want to do any changes to the infrastructure and rebuild cdk.tf.json

cdktf must be installed, run:

$ npm install -g cdktf-cli



* Version "just testing" 




### Testing ###
* You will need to set up golang before runing test follow [This quick start with terratest](https://terratest.gruntwork.io/docs/getting-started/quick-start/)

* Once golang installed, go to test folder and run:
$ go test -v 
