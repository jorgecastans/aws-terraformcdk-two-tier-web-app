#!/usr/bin/env python
from constructs import Construct
from cdktf import App, TerraformStack, TerraformOutput
from imports.aws import SnsTopic, AwsProvider
from imports.aws import Vpc, Subnet,IamRole,IamRolePolicyAttachment,IamInstanceProfile,InternetGateway,RouteTableRoute,RouteTable,RouteTableAssociation
from imports.aws import Instance, SecurityGroupIngress, SecurityGroup,Eip,NatGateway
from imports.aws import S3Bucket,LbAccessLogs,LbTargetGroup,LbTargetGroupHealthCheck,Lb,LbListenerDefaultAction,LbListener

iam_assume_role_policy_webserver = '{"Version": "2012-10-17","Statement": [{"Effect": "Allow","Principal": {"Service": "ec2.amazonaws.com"},"Action": "sts:AssumeRole"}]}'


class MyStack(TerraformStack):
    def __init__(self, scope: Construct, ns: str):
        super().__init__(scope, ns)
        euwest1_provider = AwsProvider(self, 'aws', region='eu-west-1')

    #VPC NETWORK
        vpc = Vpc(self,id='CustomVpc',cidr_block='10.0.0.0/16',provider=euwest1_provider,tags={'Name' : "CustomVPC"})

        public_subnet_a = Subnet(self,id='PublicSubnet1',cidr_block='10.0.1.0/24',vpc_id=vpc.id,availability_zone='eu-west-1a')
        public_subnet_b = Subnet(self,id='PublicSubnet2',cidr_block='10.0.2.0/24',vpc_id=vpc.id,availability_zone='eu-west-1b')

        private_subnet_a = Subnet(self,id='PrivateSubnet1',cidr_block='10.0.3.0/24',vpc_id=vpc.id,availability_zone='eu-west-1a')
        private_subnet_b = Subnet(self,id='PrivateSubnet2',cidr_block='10.0.4.0/24',vpc_id=vpc.id,availability_zone='eu-west-1b')

        #Internet Gateway and Public Routing
        ig = InternetGateway(self,id='internet-gateway',vpc_id=vpc.id)

        public_routing = RouteTableRoute(
            cidr_block='0.0.0.0/0',
            egress_only_gateway_id= '',
            gateway_id=ig.id,
            instance_id= '',
            ipv6_cidr_block = '',
            nat_gateway_id= '',
            network_interface_id= '',
            transit_gateway_id= '',
            vpc_peering_connection_id = '')

        public_route_table = RouteTable(self,id='public-route-table',vpc_id=vpc.id,route=[public_routing])

        RouteTableAssociation(self,id='association-public-subnet-a',route_table_id=public_route_table.id,subnet_id=public_subnet_a.id)
        RouteTableAssociation(self,id='association-public-subnet-b',route_table_id=public_route_table.id,subnet_id=public_subnet_b.id)
        
        #Nat GateWay and Private Routing
        
        elastic_ip = Eip(self,id='elastic-ip')
        nat_gateway = NatGateway(self, id='Nat-gateway',allocation_id=elastic_ip.id,subnet_id=public_subnet_a.id)

        private_routing = RouteTableRoute(
            cidr_block='0.0.0.0/0',
            egress_only_gateway_id= '',
            gateway_id='',
            instance_id= '',
            ipv6_cidr_block = '',
            nat_gateway_id= nat_gateway.id,
            network_interface_id= '',
            transit_gateway_id= '',
            vpc_peering_connection_id = '')

        private_route_table = RouteTable(self,id='private-route-table',vpc_id=vpc.id,route=[private_routing])

        RouteTableAssociation(self,id='association-private-subnet-a',route_table_id=private_route_table.id,subnet_id=private_subnet_a.id)
        RouteTableAssociation(self,id='association-private-subnet-b',route_table_id=private_route_table.id,subnet_id=private_subnet_b.id)

    #SECURITY GROUPS
        sg_inbound_rules_load_balancer = SecurityGroupIngress(description = "HTTP for loadbalancer",cidr_blocks=['0.0.0.0/0'],from_port=80,to_port=80,protocol='tcp',ipv6_cidr_blocks=[],prefix_list_ids=[],security_groups=[],self_attribute=False)
        sg_load_balancer = SecurityGroup(self,id='sg_load_balancer',name='sg_load_balancer',ingress=[sg_inbound_rules_load_balancer],vpc_id=vpc.id)
        
        sg_inbound_rules_web_server = SecurityGroupIngress(description = "Only trafic from loadbalancer",security_groups=[sg_load_balancer.id],from_port=80,to_port=80,protocol='tcp',ipv6_cidr_blocks=[],prefix_list_ids=[],cidr_blocks=[],self_attribute=False)
        sg_web_server = SecurityGroup(self,id='sg_web_server',name='sg_web_server',ingress=[sg_inbound_rules_web_server],vpc_id=vpc.id)

    #IAM
        iam_role_web_server = IamRole(self,id='web-server-role',name='web-server-role',assume_role_policy=iam_assume_role_policy_webserver)
        IamRolePolicyAttachment(self,id='web-server-policy',policy_arn='arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess',role=iam_role_web_server.id)

        iam_instanceprofile_web_server = IamInstanceProfile(self,id='web-server-instance-profile',role=iam_role_web_server.id)

    #S3

        s3_logs_bucket =  S3Bucket(self,id='s3-logs-bucket',bucket='web-server-terraform-logs-jc',region=euwest1_provider.region)

    #LOAD BALANCER
        
        lb_target_group = LbTargetGroup(self,id='lb-target-group',name='web-server-target-group',
        health_check=[LbTargetGroupHealthCheck(path='/',protocol='HTTP')])
        
        lb_access_log = LbAccessLogs(bucket=s3_logs_bucket.bucket,enabled=True,prefix='web-server-lb-logs')

        lb_web_server = Lb(self,id='lb-web-server',name='lb-web-server',
        access_logs=[lb_access_log],load_balancer_type='application',security_groups=[sg_load_balancer.id],
        subnets=[public_subnet_a.id,public_subnet_b.id])

        lb_listener_action = LbListenerDefaultAction(type='forward',target_group_arn=lb_target_group.arn)

        lb_listener =  LbListener(self,id='lb-listener',default_action=[lb_listener_action],load_balancer_arn=lb_web_server.arn,
        port=80,protocol='http')
        

        Instance(self, "InstanceA", ami="ami-0a7c31280fbd23a86", instance_type="t2.micro",subnet_id=public_subnet_a.id,tags={'Name' : "TerraformInstace"},iam_instance_profile=iam_instanceprofile_web_server.id)
        Instance(self, "InstanceB", ami="ami-0a7c31280fbd23a86", instance_type="t2.micro",subnet_id=public_subnet_b.id,tags={'Name' : "TerraformInstace"},iam_instance_profile=iam_instanceprofile_web_server.id)

        
        TerraformOutput(self, 'vpcId',
            value=vpc.id
        )


        # define resources here


app = App()
MyStack(app, "aws-terraformcdk-two-tier-web-app")

app.synth()

# WORK ARROUND FOR A BUG IN THE TERRAFORM CDK DUE TO BEING SELF A RESERVE WORD IN PYTHON.
# AFTER THE JSON IS FORMED, WE REPLACE THE 
# "SELF_ATTRIBUTE"(WHAT PYTHON CDK WRITES SINCE THE CLASS SEGURITYGROUP CANT HAVE A SELF PARAM) 
# WITH "SELF" (WHAT TERRAFORM NEEDS IN THE JSON).
# NOTE THIS WORK ARROUND CAN GENERATE BUG IN COMPLEX CONFIGUTARIONS. 
with open('./cdk.tf.json', 'r') as file :
  filedata = file.read()
filedata = filedata.replace('"self_attribute": false', '"self": false')
with open('./cdk.tf.json', 'w') as file:
  file.write(filedata)